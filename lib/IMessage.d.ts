export interface IMessage {
    device_id: string;
    module_id: string;
    timestamp: number;
    data: {
        key: string;
        value: any;
    };
}