<div style="text-align: center">
    <img src="logo.png" alt="plethora logo">
    <p>a nagios-like application aiming at the IOT world!</p>
</div>

<hr>

# | Types

These are common types between typescript projects

## Contributors

+ [Constant Deschietere](mailto:constant.deschietere@cpe.fr)
+ [Rudy Deal](mailto:rudy.deal@cpe.fr)
+ [Maxime Collot](mailto:maxime.collot@cpe.fr)
+ [Florian Croisot](mailto:constant.deschietere@cpe.fr)
+ [Jordan Quagliatini](mailto:jordan.quagliatini@cpe.fr)
